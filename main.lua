local calc = require "calc"
local Point = calc.Point

function love.load()
	love.window.setFullscreen(true)
	love.mouse.setVisible(false)
	print(_VERSION)
	print(love.getVersion())

	width, height = love.graphics.getDimensions()

	time = 0
	k = 0.1
	m, n = k*width, k*height

	lifeRule = love.graphics.newShader[[
		uniform vec2 texture_size;

		vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
			// Because texture coords are normalized to 1
			float dx = 1.0 / texture_size.x;
			float dy = 1.0 / texture_size.y;

			// Is it possible to represent alive neighbours matrix via some matrix products?
			vec4 alive_neighbours = 
				Texel(texture, vec2(texture_coords.x - dx, texture_coords.y - dy)) +
				Texel(texture, vec2(texture_coords.x - dx, texture_coords.y)) +
				Texel(texture, vec2(texture_coords.x - dx, texture_coords.y + dy)) +
				Texel(texture, vec2(texture_coords.x, texture_coords.y - dy)) +
				Texel(texture, vec2(texture_coords.x, texture_coords.y + dy)) +
				Texel(texture, vec2(texture_coords.x + dx, texture_coords.y - dy)) +
				Texel(texture, vec2(texture_coords.x + dx, texture_coords.y)) +
				Texel(texture, vec2(texture_coords.x + dx, texture_coords.y + dy));

			vec4 self = Texel(texture, texture_coords);
			if (self.x < 0.5) {
				if (alive_neighbours.x >= 1.5 && alive_neighbours.x <= 3.5) {
					return vec4(1.0, 1.0, 1.0, 1.0);
				}
				else return vec4(0.0, 0.0, 0.0, 1.0);
			}
			else {
				if (alive_neighbours.x >= 2.5 && alive_neighbours.x <= 3.5) {
					return vec4(1.0, 1.0, 1.0, 1.0);
				}
				else return vec4(0.0, 0.0, 0.0, 1.0);
			}
		}
	]]
	lifeRule:send("texture_size", {m, n})

	currentState = love.graphics.newCanvas(m, n)
	currentState:setFilter("nearest")

	nextState = love.graphics.newCanvas(m, n)
	nextState:setFilter("nearest")

	randomFill = love.graphics.newShader[[
		uniform vec3 seed;

		vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
			// I don't know whether this is uniform, normal or etc. distribution
			return vec4(fract(sin(dot(screen_coords, seed.xy)) * 100000 * seed.z));
		}
	]]
	randomFill:send("seed", {love.math.random(), love.math.random(), love.math.random()})

	xScaleFactor = width / m
	yScaleFactor = height / n
	angle = 0  -- in radians

	function setBgColor(p)
		love.graphics.setBackgroundColor(p.x, p.y, p.z)
	end

	function setColor(p)
		love.graphics.setColor(p.x, p.y, p.z)
	end

	function drawWithShader(drawable, shader)
		return function()
			love.graphics.setShader(shader)
				love.graphics.draw(drawable, 0, 0)
			love.graphics.setShader()
		end
	end

	setBgColor(Point.new(0,0,0))
	setColor(Point.new(255,255,255))

	currentState:renderTo(drawWithShader(nextState, randomFill))
end

function love.keypressed(key)
	keyPressed = key
end

function love.keyreleased(key)
	if key==keyPressed then keyPressed = '' end
end

function love.wheelmoved(_x,_y)
	
end

function love.update(dt)
	time = time + dt
	if keyPressed=='b' then
		local screenshot = love.graphics.newScreenshot()
		local str = os.time() .. '.png'
		screenshot:encode('png', str)
	end
end

function love.draw()
	nextState:renderTo(drawWithShader(currentState, lifeRule))
	currentState, nextState = nextState, currentState

	love.timer.sleep(1)
	love.graphics.draw(currentState, 0, 0, angle, xScaleFactor, yScaleFactor)
end
